

let http = require('http')

let users = [
		{
			"name": "Jose Marie Chan",
			"email": "jmari@mail.com"
		},
		{
			"name": "Mariah Carey",
			"email": "mariahcarey@mail.com"
		}
]

let port = 8080

const server = http.createServer(function (request,response) {

	if(request.url == '/users' && request.method == 'GET'){
		response.writeHead(200,{'Content-Type':'application/json'})
		response.write(JSON.stringify(users))
		response.end()
	} else if (request.url == '/users' && request.method == 'POST') {
		let request_body = ''

		request.on('data', function(data){
			request_body += data 
			console.log(request_body)
		})

		request.on('end', function() {
			console.log(typeof request_body)

			request_body = JSON.parse(request_body)

			let new_user = {
				"name": request_body.name,
				"email": request_body.email
			}

			users.push(new_user)
			console.log(users)


			response.writeHead(201,{'Content-Type':'application/json'})
			response.write(JSON.stringify(new_user))
			response.end()



		})


		
	}



})

server.listen(port)

console.log(`Server is now accessible at localhost: ${port}`)


